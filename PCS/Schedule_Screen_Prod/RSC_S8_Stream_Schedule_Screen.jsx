#include json2.js
#include TeamInfo.jsx

//------------------------------------------------------//
// 														//
//		  Title: RSC_Stream_Schedule_Screen.jsx		    //
//                  Author: Sennji						//
//                   Version: 4.0						//
//               Creation: 03.02.2021					//
//														//
//------------------------------------------------------//
//														//
//	Purpose: This Script shall provide the Screens 		//
//	needed for the Streams of the League over at 		//
//	https:\\twitch.tv/RSC_EU 							//
//  Version 4 now for Season 7 of the RSC 				//
//														//
//------------------------------------------------------//


//------------------------------------------------------//
//														//
//	Taking in the Settings, and Stats via JSON Files 	//
//														//
//------------------------------------------------------//

var GenInfo = loadJson("GenInfo.json");
var doc = app.activeDocument;

//------------------------------------------------------//
//														//
// Function Main 										//
//														//
//------------------------------------------------------//
//														//
// Going through all the files needed. Also giving out  //
// the alert, to give out a confirmation to the user.   //
//														//
//------------------------------------------------------//


(function main(){
	

	//dailySchedule(GenInfo.Day[0], 0);
	if(GenInfo.Day[3].Verification) {
		if(GenInfo.needWeekly) {weeklySchedule(4, doc.layerSets.getByName("WeeklyStreamSchedule 4days"));}
		
		for (var i = 0; i < 4; i++){
			dailySchedule(GenInfo.Day[i], i);
		}
	} else {

		if(GenInfo.needWeekly) {weeklySchedule(3, doc.layerSets.getByName("WeeklyStreamSchedule"));}
		
		for (var i = 0; i < 3; i++){
			dailySchedule(GenInfo.Day[i], i);
		}
	}


	alert("done!")
		
})();

function dailySchedule(day, dayNbr){
	hideAll();

	var dailyFolder = doc.layerSets.getByName("DailyStreamSchedule");
	dailyFolder.visible = true;
	var top = dailyFolder.layerSets.getByName("Top")
	top.layers[0].textItem.contents = " \rDaily Stream Schedule - Week " + GenInfo.Week + "\r"+ day.Date;
	
	var teamNames = [day.Game1Blue, day.Game2Blue, day.Game3Blue, day.Game1Red, day.Game2Red, day.Game3Red];

	for(var i = 0; i <= 2; i++) {
		if(teamNames[i] == "blank") {
			blankWriterWeekly(dailyFolder.layers[1 + i]);
		} else {
			gameWriterDaily(dailyFolder.layers[1 + i], teamNames[i], teamNames[i+3])
		}
	}

	savePNG(doc,  "Week " + GenInfo.Week + " Day " + (dayNbr + 1) + " Stream Schedule");
}

function blankWriterDaily(folder) {
	folder.visible = false;
}

function gameWriterDaily(folder, teamBlue, teamRed){

	folder.visible = true;
	var conference = folder.layerSets.getByName("Conference");
	var tierFolder = folder.layerSets.getByName("Tier");

	if(teamInfo[teamBlue][1] == teamInfo[teamRed][1]) {
		if(teamInfo[teamBlue][1] == "Ignis") {

			conference.layers[0].visible = false;
			conference.layers[1].visible = false;
			conference.layers[2].visible = true;
		} else {
			conference.layers[0].visible = false;
			conference.layers[1].visible = true;
			conference.layers[2].visible = false;
		}
	} else {
		conference.layers[0].visible = true;
		conference.layers[1].visible = false;
		conference.layers[2].visible = false;
	}
	

	var tier = teamInfo[teamBlue][2]; 
	
	for(var i = 0; i < 6; i++){
		tierFolder.layers[i].visible = false;
	}

	switch(tier) {
		case "Premier": 
			tierFolder.layers[0].visible = true; 
			conference.layers[0].visible = false;
			conference.layers[1].visible = false;
			conference.layers[2].visible = false;
			break;
		case "Elite": tierFolder.layers[1].visible = true; break;
		case "Major": tierFolder.layers[2].visible = true; break;
		case "Minor": tierFolder.layers[3].visible = true; break;
		case "Challenger": tierFolder.layers[4].visible = true; break;
		case "Prospect": tierFolder.layers[5].visible = true; break;
	}


	folder.layers[2].textItem.contents = teamBlue;
	folder.layers[4].textItem.contents = teamRed;
	var blueLogo = folder.layerSets.getByName("Top Logo");
	var redLogo = folder.layerSets.getByName("Bottom Logo");

	setTeamLogo(blueLogo, teamInfo[teamBlue][0]);
	setTeamLogo(redLogo, teamInfo[teamRed][0]);
}

function weeklySchedule(dayCount, weeklyFolder){

	hideAll();

	weeklyFolder.visible = true;
	var top = weeklyFolder.layerSets.getByName("Top");
	//General Infos at the top
	top.layers[0].textItem.contents = " \rWeekly Stream Schedule\rWeek "+ GenInfo.Week;
	
	//going through the Days
	for(var i = 1; i <= dayCount; i++) {
		var day = weeklyFolder.layers[i]
		top.layers[i].textItem.contents = GenInfo.Day[i-1].Date;

		var teamNames = [GenInfo.Day[i-1].Game1Blue, GenInfo.Day[i-1].Game2Blue, 
						 GenInfo.Day[i-1].Game3Blue, GenInfo.Day[i-1].Game1Red, 
						 GenInfo.Day[i-1].Game2Red, GenInfo.Day[i-1].Game3Red];

		for(var j = 0; j <= 2; j++) {
			if(teamNames[j] == "blank") {
				blankWriterWeekly(day.layers[j])
			} else {
				gameWriterWeekly(day.layers[j], teamNames[j], teamNames[j+3])
			}
		}
	}

	savePNG(doc,  "Week " + GenInfo.Week + " Weekly Stream Schedule");
}

function blankWriterWeekly(folder) {
	folder.visible = false;
}

function gameWriterWeekly(folder, teamBlue, teamRed){
	folder.visible = true;

	var tierTime = folder.layerSets.getByName("Tier-Time");
	var conference = tierTime.layerSets.getByName("Conference");

	if(teamInfo[teamBlue][1] == teamInfo[teamRed][1]) {
		if(teamInfo[teamBlue][1] == "Ignis") {
			conference.layers[0].visible = false;
			conference.layers[1].visible = false;
			conference.layers[2].visible = true;
		} else {
			conference.layers[0].visible = false;
			conference.layers[1].visible = true;
			conference.layers[2].visible = false;
		}
	} else {
		conference.layers[0].visible = true;
		conference.layers[1].visible = false;
		conference.layers[2].visible = false;
	}



	for(var i = 2; i<=6; i++){
		tierTime.layers[i].visible = false;
	}

	var tier = teamInfo[teamBlue][2]; 

	for(var i = 0; i < 6; i++){
		tierTime.layers[i+2].visible = false;
	}

	switch(tier) {
		case "Premier": 
			tierTime.layers[2].visible = true; 
			conference.layers[0].visible = false;
			conference.layers[1].visible = false;
			conference.layers[2].visible = false;
			break;
		case "Elite": tierTime.layers[3].visible = true; break;
		case "Major": tierTime.layers[4].visible = true; break;
		case "Minor": tierTime.layers[5].visible = true; break;
		case "Challenger": tierTime.layers[6].visible = true; break;
		case "Prospect": tierTime.layers[7].visible = true; break;
	}

	nameBlue = teamBlue;
	nameRed = teamRed;

	if(teamBlue.indexOf(" ") >= 0) {
		nameBlue = nameBlue.replace(/ /g, "\r");
		nameRed = nameRed.replace(/ /g, "\r");
		folder.layers[1].textItem.contents = nameBlue;
		folder.layers[3].textItem.contents = nameRed;
	} else {
		folder.layers[1].textItem.contents = "\r" + teamBlue;
		folder.layers[3].textItem.contents = teamRed + "\r";
	}

	var blueLogo = folder.layerSets.getByName("Left Logo");
	var redLogo = folder.layerSets.getByName("Right Logo");

	setTeamLogo(blueLogo, teamInfo[teamBlue][0]);
	setTeamLogo(redLogo, teamInfo[teamRed][0]);
}

function setTeamLogo(logoGroup, teamLogo){
	for (var i = 0; i < logoGroup.layers.length; i++) { 	
		logoGroup.layers[i].visible = false;
	}
	logoGroup.layers[teamLogo].visible = true; 
}

function hideAll(){

	//Initializing all the Groups
	var Weekly = doc.layerSets.getByName("WeeklyStreamSchedule");
	var Daily  = doc.layerSets.getByName("DailyStreamSchedule");
	var Weekly4= doc.layerSets.getByName("WeeklyStreamSchedule 4days");

	//Actually hiding them
	Weekly.visible = false;
	Daily.visible = false;
	Weekly4.visible = false;
}

function loadJson(relPath) {
	var script = new File($.fileName);
	var jsonFile = new File(script.path + '/' + relPath);

	jsonFile.open('r');
	var str = jsonFile.read();
	jsonFile.close();

	return JSON.parse(str)
}

function savePNG(doc, name) {
	var file = new File(doc.path + '/Output/' + name + '.PNG');

	var opts = new PNGSaveOptions();
	opts.quality = 10;

	doc.saveAs(file, opts, true);
}