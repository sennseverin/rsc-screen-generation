#include json2.js
#include ProducerInfo.jsx
//------------------------------------------------------//
// 														//
//		     Title: RSC_Stream_Screen.jsx		    	//
//                  Author: Sennji						//
//                   Version: 5.0						//
//               Creation: 03.02.2021					//
//				   Update: 17.08.2021					//
//------------------------------------------------------//
//														//
//	Purpose: This Script shall provide the Screens 		//
//	needed for the Streams of the League over at 		//
//	https:\\twitch.tv/RSC_EU 							//
//  Version 5 now for Season 8 of the RSC first Draft   //
//  with the Python Program to create the Screens 		//
//  more user friendly and faster     					//
//														//
//------------------------------------------------------//


//------------------------------------------------------//
//														//
//	Taking in the Settings, and Stats via JSON Files 	//
//														//
//------------------------------------------------------//

var GenInfo = loadJson("GenInfo.json");
var doc = app.activeDocument;

//------------------------------------------------------//
//														//
// Function Main 										//
//														//
//------------------------------------------------------//
//														//
// Going through all the files needed. Also giving out  //
// the alert, to give out a confirmation to the user.   //
//														//
//------------------------------------------------------//


(function main(){

	gameStorage = [GenInfo.Game1, GenInfo.Game2, GenInfo.Game3];


	for (var i = 0; i < gameStorage.length; i++){
		if((i == 0 && GenInfo.General.G1) || (i == 1 && GenInfo.General.G2) || (i == 2 && GenInfo.General.G3)){
			if(GenInfo.General.CS == 1) {doCS(gameStorage[i]);}
			if(GenInfo.General.VS == 1) {doVS(gameStorage[i]);}
			if(GenInfo.General.PS == 1) {doPS(gameStorage[i]);}
			if(GenInfo.General.TS == 1) {doTS(gameStorage[i]);}
		}
	}

	alert("done!");

})();


function doCS(gameInfo){
	
	//hiding all other groups, to make sure we get what we want
	hideAll();

	//Initialize everything we need in this function
	var CSGroup = doc.layerSets.getByName("RSC Caster Screen");

	//current Match
	var currentMatch = CSGroup.layerSets.getByName("Match");
	var currentMatchBlue = currentMatch.layerSets.getByName("CSB_Logo");
	var currentMatchRed = currentMatch.layerSets.getByName("CSR_Logo");
	var title = currentMatch.layers[2];
	var week = currentMatch.layers[6];
	var currentBlueTeam = currentMatch.layers[9];
	var currentRedTeam = currentMatch.layers[13];

	//caster Blue
	var casterBlue = CSGroup.layerSets.getByName("Caster Blue");
	var casterBluetwitterHandle = casterBlue.layers[0];
	var casterBlueName = casterBlue.layers[1];

	//caster Red
	var casterRed = CSGroup.layerSets.getByName("Caster Red");
	var casterRedtwitterHandle = casterRed.layers[0];
	var casterRedName = casterRed.layers[1];

	//Streamer
	var streamer = CSGroup.layerSets.getByName("Streamer");
	var streamerTwitterHandle = streamer.layers[1];
	var streamerName = streamer.layers[2];
	var streamerLogo = streamer.layerSets.getByName("Streamer logo");

	//Later
	var later = CSGroup.layerSets.getByName("Later");
	var after1 = later.layerSets.getByName("Match 1");
	var after2 = later.layerSets.getByName("Match 2");

	//Background
	var background = CSGroup.layerSets.getByName("Background");
	var allStarBG = background.layerSets.getByName("All Star BG");
	var leaguePlayBG = background.layerSets.getByName("League Play BG");
	var preSeasonBG = background.layerSets.getByName("Pre Season BG");
	var playOffBG = background.layerSets.getByName("Play Off BG");

	//initialisation done

	//get the CS Group to be visible
	CSGroup.visible = true;

	//set current Match Details
	title.textItem.contents = "S8 " + gameInfo.Tier + "\r" + GenInfo.General.RoundType;
	week.textItem.contents = GenInfo.General.GameRound;

	//blue Team
	currentBlueTeam.textItem.contents = gameInfo.BlueTeam;
	setTeamLogo(currentMatchBlue, gameInfo.BlueLogo);

	//red Team
	currentRedTeam.textItem.contents = gameInfo.RedTeam;
	setTeamLogo(currentMatchRed, gameInfo.RedLogo);

	//later
	later.visible = false;
	after1.visible = false;
	after2.visible = false;

	if(GenInfo.General.G1) {

		if(gameInfo.GameNbr == 1) {
			later.visible = true;
			if(GenInfo.General.G2) {
				doLaterGame(GenInfo.Game2, after1);
			}
			if(GenInfo.General.G3) {
				doLaterGame(GenInfo.Game3, after2);
			}
		}

		if(gameInfo.GameNbr == 2) {
			later.visible = true;
			if(GenInfo.General.G3) {
				doLaterGame(GenInfo.Game3, after1);
			}
		}
	} else if (GenInfo.General.G2) {

		if(gameInfo.GameNbr == 2) {
			later.visible = true;
			if(GenInfo.General.G3) {
				doLaterGame(GenInfo.Game3, after1);
			}
		}
	}
	
	//blue caster
	casterBlueName.textItem.contents = gameInfo.Caster1;
	casterBluetwitterHandle.textItem.contents = producerInfo[gameInfo.Caster1][0];

	//red caster
	casterRedName.textItem.contents = gameInfo.Caster2;
	casterRedtwitterHandle.textItem.contents = producerInfo[gameInfo.Caster2][0];

	//broadcaster
	streamerName.textItem.contents = gameInfo.Streamer;
	streamerTwitterHandle.textItem.contents = producerInfo[gameInfo.Streamer][0];
	setProducerLogo(streamerLogo, producerInfo[gameInfo.Streamer][1]);
	
	//Background Settings
	allStarBG.visible = false;
	leaguePlayBG.visible = false;
	preSeasonBG.visible = false;
	playOffBG.visible = false;
	switch(GenInfo.General.RoundType) {
		case "All-Star": allStarBG.visible = true; break;
		case "League Play": leaguePlayBG.visible = true; break;
		case "Pre-Season": preSeasonBG.visible = true; break;
		case "Play-Offs": playOffBG.visible = true; break;
		case "Play-Ins": playOffBG.visible = true; break;
	}

	savePNG(doc, gameInfo.GameNbr + "_CS_" + gameInfo.BlueTeam + "_vs_" + gameInfo.RedTeam);
}

function doVS(gameInfo){
	//hiding all other groups, to make sure we get what we want
	hideAll();

	//Initialize everything we need in this function
	var VSGroup = doc.layerSets.getByName("RSC vs Screen");

	//General Text Parts
	var VSGenText = VSGroup.layerSets.getByName("Text");
	var VSTier = VSGenText.layers[0];
	var VSRoundType = VSGenText.layers[2];
	var VSGameRound = VSGenText.layers[3];

	//Blue Team
	var VSBlueTeam = VSGroup.layerSets.getByName("Blue Team");
	//var VSBlueLogo = VSBlueTeam.layerSets.getByName("VSB_Logo");
	//var VSBlueLogoBack = VSBlueTeam.layerSets.getByName("VSBB_Logo");	
	var VSBNameLayer = VSBlueTeam.layers[0];
	var VSBlueLogo = VSBlueTeam.layerSets.getByName("Logo");

	//Red Team
	var VSRedTeam = VSGroup.layerSets.getByName("Red Team");
	//var VSRedLogo = VSRedTeam.layerSets.getByName("VSR_Logo");
	//var VSRedLogoBack = VSRedTeam.layerSets.getByName("VSBR_Logo");
	var VSRNameLayer = VSRedTeam.layers[0];
	var VSRedLogo = VSRedTeam.layerSets.getByName("Logo");

	//Backgrounds
	var VSbg = VSGroup.layerSets.getByName("Background");
	var leaguePlayBG = VSbg.layerSets.getByName("LeaguePlay");
	var playOffBG = VSbg.layerSets.getByName("Playoffs");

	//initialisation done

	//get the VS Group to be visible
	VSGroup.visible = true;

	//these following lines change the text according to the GenInfo.json
	VSBNameLayer.textItem.contents = gameInfo.BlueTeam;
	VSRNameLayer.textItem.contents = gameInfo.RedTeam;
	VSTier.textItem.contents = "Season 8 | " + gameInfo.Tier + " Tier"
	VSRoundType.textItem.contents = GenInfo.General.RoundType;
	VSGameRound.textItem.contents = GenInfo.General.GameRound;

	//these following lines determine and place the logos
	//blue team
	setTeamLogo(VSBlueLogo, gameInfo.BlueLogo);
	//setTeamLogo(VSBlueLogoBack, gameInfo.BlueLogo);

	//red Team
	setTeamLogo(VSRedLogo, gameInfo.RedLogo);
	//setTeamLogo(VSRedLogoBack, gameInfo.RedLogo);

	//Background Settings
	leaguePlayBG.visible = false;
	playOffBG.visible = false;
	switch(GenInfo.General.RoundType) {
		case "League Play": leaguePlayBG.visible = true; break;
		case "Play-Offs": playOffBG.visible = true; break;
		case "Play-Ins": playOffBG.visible = true; break;
	}

	//now it's time to save it all
	savePNG(doc, gameInfo.GameNbr + "_VS_" + gameInfo.BlueTeam + "_vs_" + gameInfo.RedTeam);
}

function doPS(gameInfo){

	//hiding all other groups, to make sure we get what we want
	hideAll();

	//Initialize everything we need in this function
	var PSGroup = doc.layerSets.getByName("RSC Player Stats");

	//Blue Team
	var PSBlueTeam = PSGroup.layerSets.getByName("Blue Team");
	var PSBNameLayer = PSBlueTeam.layers[0];
	var PSBlueLogo = PSBlueTeam.layerSets.getByName("PSB_Logo");

	//Blue Team Player
	var player1Blue = PSBlueTeam.layerSets.getByName("Player 1");
	var player2Blue = PSBlueTeam.layerSets.getByName("Player 2");
	var player3Blue = PSBlueTeam.layerSets.getByName("Player 3");
	var player4Blue = PSBlueTeam.layerSets.getByName("Player 4");

	//Red Team
	var PSRedTeam = PSGroup.layerSets.getByName("Red Team");
	var PSRNameLayer = PSRedTeam.layers[0];
	var PSRedLogo = PSRedTeam.layerSets.getByName("PSR_Logo");

	//Red Team Player
	var player1Red = PSRedTeam.layerSets.getByName("Player 1");
	var player2Red = PSRedTeam.layerSets.getByName("Player 2");
	var player3Red = PSRedTeam.layerSets.getByName("Player 3");
	var player4Red = PSRedTeam.layerSets.getByName("Player 4");
	
	//playerStorage of each Players Folder
	var playerStorage = [player1Blue, player2Blue, player3Blue, player4Blue,
						 player1Red, player2Red, player3Red, player4Red];

	//get the PS Group to be visible
	PSGroup.visible = true;

	//these following lines change the text according to the GenInfo.json
	PSBNameLayer.textItem.contents = gameInfo.BlueTeam;
	PSRNameLayer.textItem.contents = gameInfo.RedTeam;

	//these following lines determine and place the logos
	setTeamLogo(PSBlueLogo, gameInfo.BlueLogo);
	setTeamLogo(PSRedLogo, gameInfo.RedLogo);

	//these following lines put in the team stats of both teams 
	for (var y = 0; y < playerStorage.length; y++){
		playerStorage[y].visible = true;
	}

	for (var i = 0; i < playerStorage.length; i++){
		writePlayer(playerStorage[i], gameInfo.PlayerStats[i])
	}

	//now it's time to save it all
	savePNG(doc, gameInfo.GameNbr + "_PS_" + gameInfo.BlueTeam + "_vs_" + gameInfo.RedTeam);
}

function doTS(gameInfo){

	//hiding all other groups, to make sure we get what we want
	hideAll();

	//Initialize everything we need in this function
	var TSGroup = doc.layerSets.getByName("RSC Team Stats");

	//Blue Team
	var TSBlueTeam = TSGroup.layerSets.getByName("TS_Blue");
	var TSBlueLogo = TSBlueTeam.layerSets.getByName("TSB_Logo");
	var TSBNameLayer = TSBlueTeam.layers[0];

	//Red Team
	var TSRedTeam = TSGroup.layerSets.getByName("TS_Red");
	var TSRedLogo = TSRedTeam.layerSets.getByName("TSR_Logo");
	var TSRNameLayer = TSRedTeam.layers[0];

	//get the TS Group to be visible
	TSGroup.visible = true;

	//these following lines change the text according to the GenInfo.json
	TSBNameLayer.textItem.contents = gameInfo.BlueTeam;
	TSRNameLayer.textItem.contents = gameInfo.RedTeam;

	//these following lines determine and place the logos
	setTeamLogo(TSBlueLogo, gameInfo.BlueLogo);
	setTeamLogo(TSRedLogo, gameInfo.RedLogo);

	//these following lines put in the team stats of both team
	writeTeam(TSBlueTeam, gameInfo.TeamStats[0]);
	writeTeam(TSRedTeam, gameInfo.TeamStats[1]);

	//now it's time to save it all
	savePNG(doc, gameInfo.GameNbr + "_TS_" + gameInfo.BlueTeam + "_vs_" + gameInfo.RedTeam);
}

function setTeamLogo(logoGroup, teamLogo){
	for (var i = 0; i < logoGroup.layers.length; i++) { 	
		logoGroup.layers[i].visible = false;
	}
	logoGroup.layers[teamLogo].visible = true; 
}

function setProducerLogo(logo, producerLogo){
	for (var i = 0; i < logo.layers.length; i++) {
		logo.layers[i].visible = false;
	}
	logo.layers[producerLogo].visible = true;
}

function doLaterGame(game, folder) {

	var time = folder.layers[0];
	var blueLogo = folder.layerSets.getByName("CSLB_Logo");
	var redLogo = folder.layerSets.getByName("CSLR_Logo");

	folder.visible = true;
	
	var laterTime = game.Time 
	time.textItem.contents = game.Time + " CEST";
	setTeamLogo(blueLogo, game.BlueLogo);
	setTeamLogo(redLogo, game.RedLogo);	
}

function writePlayer(player, list) {
	player.layers[0].textItem.contents = list.Name;
	player.layers[1].textItem.contents = list.Games;
	player.layers[2].textItem.contents = list.Goals;
	player.layers[3].textItem.contents = list.Assists;
	player.layers[4].textItem.contents = list.Saves;
	player.layers[5].textItem.contents = list.ShotPerc;
	player.layers[6].textItem.contents = list.WinPerc;
}

function writeTeam(team, list){
	team.layers[2].textItem.contents = list.Position;
	team.layers[3].textItem.contents = list.GP;
	team.layers[4].textItem.contents = list.Wins;
	team.layers[5].textItem.contents = list.Losses;
	team.layers[6].textItem.contents = list.GF;
	team.layers[7].textItem.contents = list.GA;
	team.layers[8].textItem.contents = list.SF;
	team.layers[9].textItem.contents = list.SA;
	team.layers[10].textItem.contents = list.SP;
}

function hideAll(){

	//Initializing all the Groups
	var TSGroup = doc.layerSets.getByName("RSC Team Stats");
	var PSGroup = doc.layerSets.getByName("RSC Player Stats");
	var CSGroup = doc.layerSets.getByName("RSC Caster Screen");
	var VSGroup = doc.layerSets.getByName("RSC vs Screen");
	var OLGroup = doc.layerSets.getByName("RSC Overlay");
	
	//Actually hiding them
	TSGroup.visible = false;
	PSGroup.visible = false;
	CSGroup.visible = false;
	VSGroup.visible = false;
	OLGroup.visible = false;
}

function loadJson(relPath) {
	var script = new File($.fileName);
	var jsonFile = new File(script.path + '/' + relPath);

	jsonFile.open('r');
	var str = jsonFile.read();
	jsonFile.close();

	return JSON.parse(str)
}

function savePNG(doc, name) {
	var file = new File(doc.path + '/Output/' + name + '.PNG');

	var opts = new PNGSaveOptions();
	opts.quality = 10;

	doc.saveAs(file, opts, true);
}