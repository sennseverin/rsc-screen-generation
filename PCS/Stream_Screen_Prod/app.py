from tkinter import *
import json
import APILogic

root = Tk()
root.title('RSC Season 8 Graphic Maker')
root.geometry("1200x600")


teamData = {"N/A ALI":      [0, "Premier"],
            "Diamond Dust": [0, "Elite"],
            "Genesis":      [0, "Major"],
            "Prominence":   [0, "Minor"],
            "Epsilon":      [0, "Challenger"],
            "Gemini Storm": [0, "Prospect"],

            "N/A ASTR":     [1, "Premier"],
            "Asteroid":     [1, "Elite"],
            "Meteor":       [1, "Major"],
            "Comet":        [1, "Minor"],
            "Satellite":    [1, "Challenger"],
            "Orbit":        [1, "Prospect"],

            "Oreos":          [2, "Premier"],
            "Jaffa Cakes":    [2, "Elite"],
            "Jammie Dodgers": [2, "Major"],
            "Custard Creams": [2, "Minor"],
            "Party Rings":    [2, "Challenger"],
            "Digestives":     [2, "Prospect"],

            "Warlocks":       [3, "Premier"],
            "Oracles":        [3, "Elite"],
            "Necromancers":   [3, "Major"],
            "Sorcerers":      [3, "Minor"],
            "Pyromancers":    [3, "Challenger"],
            "Mages":          [3, "Prospect"],

            "Vulcan":          [4, "Premier"],
            "Phoenix":         [4, "Elite"],
            "Bladesmiths":     [4, "Major"],
            "Journeymen":      [4, "Minor"],
            "Apprentices":     [4, "Challenger"],
            "Hot Metal":       [4, "Prospect"],

            "not Available1":  [5, "Premier"],
            "Fearless Alpha":  [5, "Elite"],
            "Fearless Delta":  [5, "Major"],
            "Fearless Echo":   [5, "Minor"],
            "Fearless Kilo":   [5, "Challenger"],
            "Fearless Bravo":  [5, "Prospect"],

            "Vibe Vibin":  [6, "Premier"],
            "Centurions":  [6, "Elite"],
            "Gladiators":  [6, "Major"],
            "Vanguards":   [6, "Minor"],
            "Crusaders":   [6, "Challenger"],
            "Cardinals":   [6, "Prospect"],

            "Flame":    [7, "Premier"],
            "Inferno":  [7, "Elite"],
            "Blaze":    [7, "Major"],
            "Ember":    [7, "Minor"],
            "Spark":    [7, "Challenger"],
            "Kindling": [7, "Prospect"],

            "Opera GX":      [8, "Premier"],
            "Google Chrome": [8, "Elite"],
            "FireFox":       [8, "Major"],
            "Safari":        [8, "Minor"],
            "Yahoo":         [8, "Challenger"],
            "Bing":          [8, "Prospect"],

            "Luna eSports": [9, "Premier"],
            "Titan":        [9, "Elite"],
            "Prometheus":   [9, "Major"],
            "Hydra":        [9, "Minor"],
            "Atlas":        [9, "Challenger"],
            "Oberon":       [9, "Prospect"],

            "N/A MOH":  [10, "Premier"],
            "Hades":    [10, "Elite"],
            "Thanatos": [10, "Major"],
            "Erebos":   [10, "Minor"],
            "Orcus":    [10, "Challenger"],
            "Tartarus": [10, "Prospect"],

            "Overburn":    [11, "Premier"],
            "Overwatch": [11, "Elite"],
            "Overtake":  [11, "Major"],
            "Overkill":  [11, "Minor"],
            "Overrule":  [11, "Challenger"],
            "Overturn":  [11, "Prospect"],

            "Finale":    [12, "Premier"],
            "Nocturne":  [12, "Elite"],
            "Rhapsody":  [12, "Major"],
            "Concerto":  [12, "Minor"],
            "Chorus":    [12, "Challenger"],
            "Prelude":   [12, "Prospect"],

            "N/A OX":      [13, "Premier"],
            "Chianina":    [13, "Elite"],
            "Anjou":       [13, "Major"],
            "Montbeliarde":[13, "Minor"],
            "Parthenais":  [13, "Challenger"],
            "Limousin":    [13, "Prospect"],

            "N/A SHMN": [14, "Premier"],
            "Dogon":    [14, "Elite"],
            "Zulu":     [14, "Major"],
            "Yoruba":   [14, "Minor"],
            "Maasai":   [14, "Challenger"],
            "Karo":     [14, "Prospect"],

            "N/A SOL":   [15, "Premier"],
            "Iovi":      [15, "Elite"],
            "Saturnia":  [15, "Major"],
            "Uranum":    [15, "Minor"],
            "Neptunium": [15, "Challenger"],
            "Terra":     [15, "Prospect"],

            "Knull":    [16, "Premier"],
            "Carnage":  [16, "Elite"],
            "Venom":    [16, "Major"],
            "Toxin":    [16, "Minor"],
            "Scorn":    [16, "Challenger"],
            "Exolon":   [16, "Prospect"],

            "N/A UWU":       [17, "Premier"],
            "Rias Legion":   [17, "Elite"],
            "Megumin Cult":  [17, "Major"],
            "Erina Tribe":   [17, "Minor"],
            "Nezuko Gang":   [17, "Challenger"],
            "Maika Faction": [17, "Prospect"],

            }


def process():

    aList = {"GameRound": textBoxGameRound.get(), "RoundType": textBoxRoundType.get(),
             "CS": varCS.get(), "VS": varVS.get(), "PS": varPS.get(), "TS": varTS.get(),
             "G1": varG1.get(), "G2": varG2.get(), "G3": varG3.get()}

    jsonFile = open("GenInfo.json", "w")
    jsonString = json.dumps(aList)
    jsonFile.write('{"General": ')
    jsonFile.write(jsonString)

    print(varG1.get())
    print(varG2.get())
    print(varG3.get())

    if(varG1.get()):

        game1PS = APILogic.getPS(textBoxGame1Blue.get(), textBoxGame1Red.get(), teamData[textBoxGame1Blue.get()][1])
        game1TS = APILogic.getTS(textBoxGame1Blue.get(), textBoxGame1Red.get(), teamData[textBoxGame1Blue.get()][1])

        aList = {"BlueTeam": textBoxGame1Blue.get(), "RedTeam": textBoxGame1Red.get(),
                 "BlueLogo": teamData[textBoxGame1Blue.get()][0],
                 "RedLogo": teamData[textBoxGame1Red.get()][0], "Caster1": textBoxCasterGame11.get(),
                 "Caster2": textBoxCasterGame12.get(), "Streamer": textBoxStreamer1.get(),
                 "Tier": teamData[textBoxGame1Blue.get()][1],
                 "GameNbr": 1, "Time": "20:00", "GameOn": 1,
                 "PlayerStats": [
                     {"Name": game1PS[0], "Games": game1PS[1], "Goals": game1PS[2], "Assists": game1PS[3],
                      "Saves": game1PS[4], "ShotPerc": game1PS[5], "WinPerc": game1PS[6]},
                     {"Name": game1PS[7], "Games": game1PS[8], "Goals": game1PS[9], "Assists": game1PS[10],
                      "Saves": game1PS[11], "ShotPerc": game1PS[12], "WinPerc": game1PS[13]},
                     {"Name": game1PS[14], "Games": game1PS[15], "Goals": game1PS[16], "Assists": game1PS[17],
                      "Saves": game1PS[18], "ShotPerc": game1PS[19], "WinPerc": game1PS[20]},
                     {"Name": game1PS[21], "Games": game1PS[22], "Goals": game1PS[23], "Assists": game1PS[24],
                      "Saves": game1PS[25], "ShotPerc": game1PS[26], "WinPerc": game1PS[27]},
                     {"Name": game1PS[28], "Games": game1PS[29], "Goals": game1PS[30], "Assists": game1PS[31],
                      "Saves": game1PS[32], "ShotPerc": game1PS[33], "WinPerc": game1PS[34]},
                     {"Name": game1PS[35], "Games": game1PS[36], "Goals": game1PS[37], "Assists": game1PS[38],
                      "Saves": game1PS[39], "ShotPerc": game1PS[40], "WinPerc": game1PS[41]},
                     {"Name": game1PS[42], "Games": game1PS[43], "Goals": game1PS[44], "Assists": game1PS[45],
                      "Saves": game1PS[46], "ShotPerc": game1PS[47], "WinPerc": game1PS[48]},
                     {"Name": game1PS[49], "Games": game1PS[50], "Goals": game1PS[51], "Assists": game1PS[52],
                      "Saves": game1PS[53], "ShotPerc": game1PS[54], "WinPerc": game1PS[55]}],
                 "TeamStats": [
                     {"Position": game1TS[0], "GP": game1TS[1], "Wins": game1TS[2], "Losses": game1TS[3],
                      "GF": game1TS[4], "GA": game1TS[5], "SF": game1TS[6], "SA": game1TS[7], "SP": game1TS[8]},
                     {"Position": game1TS[9], "GP": game1TS[10], "Wins": game1TS[11], "Losses": game1TS[12],
                      "GF": game1TS[13], "GA": game1TS[14], "SF": game1TS[15], "SA": game1TS[16], "SP": game1TS[17]}]
                 }
        jsonString = json.dumps(aList)
        jsonFile.write(',\n"Game1": ')
        jsonFile.write(jsonString)

    if(varG2.get()):

        game2PS = APILogic.getPS(textBoxGame2Blue.get(), textBoxGame2Red.get(), teamData[textBoxGame2Blue.get()][1])
        game2TS = APILogic.getTS(textBoxGame2Blue.get(), textBoxGame2Red.get(), teamData[textBoxGame2Blue.get()][1])

        aList = {"BlueTeam": textBoxGame2Blue.get(), "RedTeam": textBoxGame2Red.get(),
                 "BlueLogo": teamData[textBoxGame2Blue.get()][0],
                 "RedLogo": teamData[textBoxGame2Red.get()][0],
                 "Caster1": textBoxCasterGame21.get(), "Caster2": textBoxCasterGame22.get(),
                 "Streamer": textBoxStreamer2.get(),
                 "Tier": teamData[textBoxGame2Blue.get()][1],
                 "GameNbr": 2, "Time": "21:00", "GameOn": 1,
                 "PlayerStats": [
                     {"Name": game2PS[0], "Games": game2PS[1], "Goals": game2PS[2], "Assists": game2PS[3],
                      "Saves": game2PS[4], "ShotPerc": game2PS[5], "WinPerc": game2PS[6]},
                     {"Name": game2PS[7], "Games": game2PS[8], "Goals": game2PS[9], "Assists": game2PS[10],
                      "Saves": game2PS[11], "ShotPerc": game2PS[12], "WinPerc": game2PS[13]},
                     {"Name": game2PS[14], "Games": game2PS[15], "Goals": game2PS[16], "Assists": game2PS[17],
                      "Saves": game2PS[18], "ShotPerc": game2PS[19], "WinPerc": game2PS[20]},
                     {"Name": game2PS[21], "Games": game2PS[22], "Goals": game2PS[23], "Assists": game2PS[24],
                      "Saves": game2PS[25], "ShotPerc": game2PS[26], "WinPerc": game2PS[27]},
                     {"Name": game2PS[28], "Games": game2PS[29], "Goals": game2PS[30], "Assists": game2PS[31],
                      "Saves": game2PS[32], "ShotPerc": game2PS[33], "WinPerc": game2PS[34]},
                     {"Name": game2PS[35], "Games": game2PS[36], "Goals": game2PS[37], "Assists": game2PS[38],
                      "Saves": game2PS[39], "ShotPerc": game2PS[40], "WinPerc": game2PS[41]},
                     {"Name": game2PS[42], "Games": game2PS[43], "Goals": game2PS[44], "Assists": game2PS[45],
                      "Saves": game2PS[46], "ShotPerc": game2PS[47], "WinPerc": game2PS[48]},
                     {"Name": game2PS[49], "Games": game2PS[50], "Goals": game2PS[51], "Assists": game2PS[52],
                      "Saves": game2PS[53], "ShotPerc": game2PS[54], "WinPerc": game2PS[55]}],
                 "TeamStats": [
                     {"Position": game2TS[0], "GP": game2TS[1], "Wins": game2TS[2], "Losses": game2TS[3],
                      "GF": game2TS[4], "GA": game2TS[5], "SF": game2TS[6], "SA": game2TS[7], "SP": game2TS[8]},
                     {"Position": game2TS[9], "GP": game2TS[10], "Wins": game2TS[11], "Losses": game2TS[12],
                      "GF": game2TS[13], "GA": game2TS[14], "SF": game2TS[15], "SA": game2TS[16], "SP": game2TS[17]}]
                 }
        jsonString = json.dumps(aList)
        jsonFile.write(',\n"Game2": ')
        jsonFile.write(jsonString)

    if(varG3.get()):

        game3PS = APILogic.getPS(textBoxGame3Blue.get(), textBoxGame3Red.get(), teamData[textBoxGame3Blue.get()][1])
        game3TS = APILogic.getTS(textBoxGame3Blue.get(), textBoxGame3Red.get(), teamData[textBoxGame3Blue.get()][1])

        aList = {"BlueTeam": textBoxGame3Blue.get(), "RedTeam": textBoxGame3Red.get(),
                 "BlueLogo": teamData[textBoxGame3Blue.get()][0],
                 "RedLogo": teamData[textBoxGame3Red.get()][0],
                 "Caster1": textBoxCasterGame31.get(), "Caster2": textBoxCasterGame32.get(),
                 "Streamer": textBoxStreamer3.get(), "Tier": teamData[textBoxGame3Blue.get()][1],
                 "GameNbr": 3, "Time": "22:00", "GameOn": 1,
                 "PlayerStats": [
                     {"Name": game3PS[0], "Games": game3PS[1], "Goals": game3PS[2], "Assists": game3PS[3],
                      "Saves": game3PS[4], "ShotPerc": game3PS[5], "WinPerc": game3PS[6]},
                     {"Name": game3PS[7], "Games": game3PS[8], "Goals": game3PS[9], "Assists": game3PS[10],
                      "Saves": game3PS[11], "ShotPerc": game3PS[12], "WinPerc": game3PS[13]},
                     {"Name": game3PS[14], "Games": game3PS[15], "Goals": game3PS[16], "Assists": game3PS[17],
                      "Saves": game3PS[18], "ShotPerc": game3PS[19], "WinPerc": game3PS[20]},
                     {"Name": game3PS[21], "Games": game3PS[22], "Goals": game3PS[23], "Assists": game3PS[24],
                      "Saves": game3PS[25], "ShotPerc": game3PS[26], "WinPerc": game3PS[27]},
                     {"Name": game3PS[28], "Games": game3PS[29], "Goals": game3PS[30], "Assists": game3PS[31],
                      "Saves": game3PS[32], "ShotPerc": game3PS[33], "WinPerc": game3PS[34]},
                     {"Name": game3PS[35], "Games": game3PS[36], "Goals": game3PS[37], "Assists": game3PS[38],
                      "Saves": game3PS[39], "ShotPerc": game3PS[40], "WinPerc": game3PS[41]},
                     {"Name": game3PS[42], "Games": game3PS[43], "Goals": game3PS[44], "Assists": game3PS[45],
                      "Saves": game3PS[46], "ShotPerc": game3PS[47], "WinPerc": game3PS[48]},
                     {"Name": game3PS[49], "Games": game3PS[50], "Goals": game3PS[51], "Assists": game3PS[52],
                      "Saves": game3PS[53], "ShotPerc": game3PS[54], "WinPerc": game3PS[55]}],
                 "TeamStats": [
                     {"Position": game3TS[0], "GP": game3TS[1], "Wins": game3TS[2], "Losses": game3TS[3],
                      "GF": game3TS[4], "GA": game3TS[5], "SF": game3TS[6], "SA": game3TS[7], "SP": game3TS[8]},
                     {"Position": game3TS[9], "GP": game3TS[10], "Wins": game3TS[11], "Losses": game3TS[12],
                      "GF": game3TS[13], "GA": game3TS[14], "SF": game3TS[15], "SA": game3TS[16], "SP": game3TS[17]}]
                 }
        jsonString = json.dumps(aList)
        jsonFile.write(',\n"Game3": ')
        jsonFile.write(jsonString)

    jsonFile.write("}")
    jsonFile.close()

    print("your GenInfo.json is ready")

    quit()

labelColumn2Space = Label(root, text ="             ")
labelColumn5Space = Label(root, text ="             ")

labelGame1Blue = Label(root, text="Insert Series 1 Blue Team: ")
labelGame1Red  = Label(root, text="Insert Series 1 Red Team: ")
labelGame2Blue = Label(root, text="Insert Series 2 Blue Team: ")
labelGame2Red  = Label(root, text="Insert Series 2 Red Team: ")
labelGame3Blue = Label(root, text="Insert Series 3 Blue Team: ")
labelGame3Red  = Label(root, text="Insert Series 3 Red Team: ")

textBoxGame1Blue = Entry(root, width = 30)
textBoxGame1Red  = Entry(root, width = 30)
textBoxGame2Blue = Entry(root, width = 30)
textBoxGame2Red  = Entry(root, width = 30)
textBoxGame3Blue = Entry(root, width = 30)
textBoxGame3Red  = Entry(root, width = 30)

labelCasterGame11 = Label(root, text="Insert Caster for Series 1: ")
labelCasterGame12 = Label(root, text="Insert Caster for Series 1: ")
labelCasterGame21 = Label(root, text="Insert Caster for Series 2: ")
labelCasterGame22 = Label(root, text="Insert Caster for Series 2: ")
labelCasterGame31 = Label(root, text="Insert Caster for Series 3: ")
labelCasterGame32 = Label(root, text="Insert Caster for Series 3: ")

textBoxCasterGame11 = Entry(root, width = 30)
textBoxCasterGame12 = Entry(root, width = 30)
textBoxCasterGame21 = Entry(root, width = 30)
textBoxCasterGame22 = Entry(root, width = 30)
textBoxCasterGame31 = Entry(root, width = 30)
textBoxCasterGame32 = Entry(root, width = 30)

labelStreamer1 = Label(root, text="Insert the Streamer for Series 1: ")
labelStreamer2 = Label(root, text="Insert the Streamer for Series 2: ")
labelStreamer3 = Label(root, text="Insert the Streamer for Series 3: ")
labelGameRound = Label(root, text="What Round of Playoffs or Week is it?")
labelRoundType = Label(root, text="What Stage of the League are we at? ")

textBoxStreamer1 = Entry(root, width = 30)
textBoxStreamer2 = Entry(root, width = 30)
textBoxStreamer3 = Entry(root, width = 30)
textBoxGameRound = Entry(root, width = 30)
textBoxRoundType = Entry(root, width = 30)

varCS = BooleanVar()
varVS = BooleanVar()
varPS = BooleanVar()
varTS = BooleanVar()

checkBoxCS = Checkbutton(root, onvalue= 1, text="Caster Screen?", variable=varCS)
checkBoxVS = Checkbutton(root, onvalue= 1, text="Versus Screen?", variable=varVS)
checkBoxPS = Checkbutton(root, onvalue= 1, text="Player Stats?", variable=varPS)
checkBoxTS = Checkbutton(root, onvalue= 1, text="Team Stats?", variable=varTS)

varG1 = BooleanVar()
varG2 = BooleanVar()
varG3 = BooleanVar()

checkBoxG1 = Checkbutton(root, onvalue= 1, text="Game 1", variable=varG1)
checkBoxG2 = Checkbutton(root, onvalue= 1, text="Game 2", variable=varG2)
checkBoxG3 = Checkbutton(root, onvalue= 1, text="Game 3", variable=varG3)

############################################################################################
#Putting on Screen
############################################################################################


labelGame1Blue.grid(row = 0, column = 0)
labelGame1Red.grid(row = 1,  column = 0)
labelGame2Blue.grid(row = 2, column = 0)
labelGame2Red.grid(row = 3,  column = 0)
labelGame3Blue.grid(row = 4, column = 0)
labelGame3Red.grid(row = 5,  column = 0)

textBoxGame1Blue.grid(row = 0, column = 1)
textBoxGame1Red.grid(row = 1,  column = 1)
textBoxGame2Blue.grid(row = 2, column = 1)
textBoxGame2Red.grid(row = 3,  column = 1)
textBoxGame3Blue.grid(row = 4, column = 1)
textBoxGame3Red.grid(row = 5,  column = 1)

labelColumn2Space.grid(row = 0, column = 2)

labelCasterGame11.grid(row = 0, column = 3)
labelCasterGame12.grid(row = 1, column = 3)
labelCasterGame21.grid(row = 2, column = 3)
labelCasterGame22.grid(row = 3, column = 3)
labelCasterGame31.grid(row = 4, column = 3)
labelCasterGame32.grid(row = 5, column = 3)

textBoxCasterGame11.grid(row = 0, column = 4)
textBoxCasterGame12.grid(row = 1, column = 4)
textBoxCasterGame21.grid(row = 2, column = 4)
textBoxCasterGame22.grid(row = 3, column = 4)
textBoxCasterGame31.grid(row = 4, column = 4)
textBoxCasterGame32.grid(row = 5, column = 4)

labelColumn5Space.grid(row = 0, column = 5)

labelStreamer1.grid(row = 0, column = 6)
labelStreamer2.grid(row = 1, column = 6)
labelStreamer3.grid(row = 2, column = 6)
labelGameRound.grid(row = 3, column = 6)
labelRoundType.grid(row = 4, column = 6)

textBoxStreamer1.grid(row = 0, column = 7)
textBoxStreamer2.grid(row = 1, column = 7)
textBoxStreamer3.grid(row = 2, column = 7)
textBoxGameRound.grid(row = 3, column = 7)
textBoxRoundType.grid(row = 4, column = 7)

checkBoxCS.grid(row = 6, column = 7)
checkBoxVS.grid(row = 7, column = 7)
checkBoxPS.grid(row = 8, column = 7)
checkBoxTS.grid(row = 9, column = 7)

checkBoxG1.grid(row = 6, column = 1)
checkBoxG2.grid(row = 7, column = 1)
checkBoxG3.grid(row = 8, column = 1)

myButton = Button(root, text="DO IT!", command= process)
myButton.grid(row = 6, column = 4)

root.mainloop()
