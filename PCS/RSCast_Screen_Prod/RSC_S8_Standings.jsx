#include json2.js
#include TeamInfo.jsx

var Standings = loadJson("Standings.json");
var OldStandings = loadJson("Standings_LW.json");
var doc = app.activeDocument;

(function main(){
	
	//doStaPrem();
	//doStanding("Elite");
	//doStanding("Major");
	//doStanding("Minor");
	//doStanding("Challenger");
	doStanding("Prospect");
	doStaFran();
	
	alert("done!");
	
})();

function doStaFran() {
	var premier = doc.layerSets.getByName("Premier");
	premier.visible = false;
	var confTier = doc.layerSets.getByName("Conference Tiers");
	confTier.visible = false;
	var franchise = doc.layerSets.getByName("Franchise");
	franchise.visible = true;

	var fraGroup = franchise.layerSets.getByName("MagicZone");
	var arrows = franchise.layerSets.getByName("Arrows");

	for(var i = 0; i < 18; i++){
		writeStanding(fraGroup.layers[i], Standings.Franchise,  i);	
		setArrow(arrows.layers[i], Standings.Franchise[i][0], OldStandings.Franchise, i, arrows.layers.length);
	}

	savePNG(doc, "Franchise Standings");

}

function doStaPrem() {
	var premier = doc.layerSets.getByName("Premier");
	premier.visible = true;
	var confTier = doc.layerSets.getByName("Conference Tiers");
	confTier.visible = false;
	var franchise = doc.layerSets.getByName("Franchise");
	franchise.visible = false;

	var premGroup = premier.layerSets.getByName("MagicZone");
	var arrows = premier.layerSets.getByName("Arrows");

	for(var i = 0; i < 10; i++){
		writeStanding(premGroup.layers[i], Standings.Premier, i);	
		setArrow(arrows.layers[i], Standings.Premier[i][0], OldStandings.Premier, i, arrows.layers.length);
	}

	savePNG(doc, "Premier Standings");

}

function doStanding(tier) {
	var premier = doc.layerSets.getByName("Premier");
	premier.visible = false;
	var confTier = doc.layerSets.getByName("Conference Tiers");
	confTier.visible = true;
	var franchise = doc.layerSets.getByName("Franchise");
	franchise.visible = false;
	var bg = confTier.layerSets.getByName("Background");
	var tiles = bg.layerSets.getByName("Tiles");
	var tierTiles = tiles.layerSets.getByName("Tier Tiles");
	var names = bg.layerSets.getByName("Names");
	var ignis = bg.layerSets.getByName("Ignis");
	var glacies = bg.layerSets.getByName("Glacies");



	for(var i = 0; i < 6; i++){
		tierTiles.layers[i].visible = false;
	}

	tierTiles.layerSets.getByName(tier).visible = true;
	names.layers[0].textItem.contents = tier + " Standings"

	switch(tier) {
		case "Elite": data = [Standings.EliteG, Standings.EliteI, OldStandings.EliteG, OldStandings.EliteI]; break;
		case "Major": data = [Standings.MajorG, Standings.MajorI, OldStandings.MajorG, OldStandings.MajorI]; break;
		case "Minor": data = [Standings.MinorG, Standings.MinorI, OldStandings.MinorG, OldStandings.MinorI]; break;
		case "Challenger": data = [Standings.ChallG, Standings.ChallI, OldStandings.ChallG, OldStandings.ChallI]; break;
		case "Prospect": data = [Standings.ProspG, Standings.ProspI, OldStandings.ProspG, OldStandings.ProspI]; break;
	}

	var tierGroup = confTier.layerSets.getByName("MagicZone");
	var arrows = confTier.layerSets.getByName("Arrows");
	
	ignis.visible = false;
	glacies.visible = true;

	for(var i = 0; i < 9; i++){
		writeStanding(tierGroup.layers[i], data[0], i);	
		setArrow(arrows.layers[i], data[0][i][0], data[2], i, arrows.layers.length);
	}

	//Arrow Stuff

	savePNG(doc, tier + " Glacies Standings");

	
	ignis.visible = true;
	glacies.visible = false;

	for(var i = 0; i <= 8; i++){
		writeStanding(tierGroup.layers[i], data[1], i);	
		setArrow(arrows.layers[i], data[1][i][0], data[3], i, arrows.layers.length);
	}

	//Arrow Stuff

	savePNG(doc, tier + " Ignis Standings");
}

function setArrow(group, teamStats, oldTeamStats, iteration, sizeOf) {
	
	var oldStandings = []
	var newPos = iteration + 1;
	var oldPos = 0;

	for(var i = 0; i < sizeOf; i++) {
		//alert(oldTeamStats[i][0]);
		if(oldTeamStats[i][0] == teamStats) {
			oldPos = i + 1;
		}
	}

	group.layers[0].visible = false;
	group.layers[1].visible = false;
	group.layers[2].visible = false;

	if(newPos < oldPos) {
		group.layers[0].visible = true;
	} else if(newPos == oldPos) {
		group.layers[1].visible = true;
	} else {
		group.layers[2].visible = true;
	}

}

function writeStanding(group, information, number) {
	group.layers[0].textItem.contents = number + 1;
	setTeamLogo(group.layers[1], teamInfo[information[number][0]][0]);
	group.layers[2].textItem.contents = information[number][0];
	group.layers[3].textItem.contents = information[number][1];
	group.layers[4].textItem.contents = information[number][2];
	group.layers[5].textItem.contents = information[number][3];
	group.layers[6].textItem.contents = information[number][4];
	group.layers[7].textItem.contents = information[number][5];
	group.layers[8].textItem.contents = information[number][6];
	group.layers[9].textItem.contents = information[number][7];
}

function setTeamLogo(logoGroup, teamLogo){
	for (var i = 0; i < logoGroup.layers.length; i++) { 	
		logoGroup.layers[i].visible = false;
	}
	logoGroup.layers[teamLogo].visible = true; 
}

function loadJson(relPath) {
	var script = new File($.fileName);
	var jsonFile = new File(script.path + '/' + relPath);

	jsonFile.open('r');
	var str = jsonFile.read();
	jsonFile.close();

	return JSON.parse(str)
}

function savePNG(doc, name) {
	var file = new File(doc.path + '/Output/' + name + '.PNG');

	var opts = new PNGSaveOptions();
	opts.quality = 10;

	doc.saveAs(file, opts, true);
}