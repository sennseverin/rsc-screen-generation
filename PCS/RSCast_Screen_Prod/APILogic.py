from pprint import pprint
import json
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("creds.json", scope)

client = gspread.authorize(creds)

tierSheet = {"Premier": [client.open("1) Premier Stats").get_worksheet(1),
                         client.open("RSC EU S8 Database").get_worksheet(4),
                         client.open("1) Premier Stats").get_worksheet(2)],
             "Elite": [client.open("2) Elite Stats").get_worksheet(1),
                       client.open("RSC EU S8 Database").get_worksheet(5),
                       client.open("2) Elite Stats").get_worksheet(2)],
             "Major": [client.open("3) Major Stats").get_worksheet(1),
                       client.open("RSC EU S8 Database").get_worksheet(6),
                       client.open("3) Major Stats").get_worksheet(2)],
             "Minor": [client.open("4) Minor Stats").get_worksheet(1),
                       client.open("RSC EU S8 Database").get_worksheet(7),
                       client.open("4) Minor Stats").get_worksheet(2)],
             "Challenger": [client.open("5) Challenger Stats").get_worksheet(1),
                            client.open("RSC EU S8 Database").get_worksheet(8),
                            client.open("5) Challenger Stats").get_worksheet(2)],
             "Prospect": [client.open("6) Prospect Stats").get_worksheet(1),
                          client.open("RSC EU S8 Database").get_worksheet(9),
                          client.open("6) Prospect Stats").get_worksheet(2)],}


def getScheduleResults(week):

    premResultsValue = "B" + str(3+(6*(week-1))) + ":G" + str(7+(6*(week-1)))
    premScheduleValue = "B" + str(3+(6*(week))) + ":G" + str(7+(6*(week)))

    tierResultsValue = "B" + str(3+(19*(week-1))) + ":G" + str(20+(19*(week-1)))
    tierScheduleValue = "B" + str(3 + (19 * (week))) + ":G" + str(20 + (19 * week))

    premResults = client.open("1) S8 Premier BC Stats").get_worksheet(0).get_values(premResultsValue)
    premSchedule = client.open("1) S8 Premier BC Stats").get_worksheet(0).get_values(premScheduleValue)
    eliteResults = client.open("2) S8 Elite BC Stats").get_worksheet(0).get_values(tierResultsValue)
    eliteSchedule = client.open("2) S8 Elite BC Stats").get_worksheet(0).get_values(tierScheduleValue)
    majorResults = client.open("3) S8 Major BC Stats").get_worksheet(0).get_values(tierResultsValue)
    majorSchedule = client.open("3) S8 Major BC Stats").get_worksheet(0).get_values(tierScheduleValue)
    minorResults = client.open("4) S8 Minor BC Stats").get_worksheet(0).get_values(tierResultsValue)
    minorSchedule = client.open("4) S8 Minor BC Stats").get_worksheet(0).get_values(tierScheduleValue)
    challResults = client.open("5) S8 Challenger BC Stats").get_worksheet(0).get_values(tierResultsValue)
    challSchedule = client.open("5) S8 Challenger BC Stats").get_worksheet(0).get_values(tierScheduleValue)
    prospResults = client.open("6) S8 Prospect BC Stats").get_worksheet(0).get_values(tierResultsValue)
    prospSchedule = client.open("6) S8 Prospect BC Stats").get_worksheet(0).get_values(tierScheduleValue)

    jsonFile = open("WeeklyResults.json", "w")

    jsonFile.write('{"Premier": [')
    writeSR(jsonFile, premResults)
    jsonFile.write('],')

    jsonFile.write('"Elite": [')
    writeSR(jsonFile, eliteResults)
    jsonFile.write('],')

    jsonFile.write('"Major": [')
    writeSR(jsonFile, majorResults)
    jsonFile.write('],')

    jsonFile.write('"Minor": [')
    writeSR(jsonFile, minorResults)
    jsonFile.write('],')

    jsonFile.write('"Challenger": [')
    writeSR(jsonFile, challResults)
    jsonFile.write('],')

    jsonFile.write('"Prospect": [')
    writeSR(jsonFile, prospResults)
    jsonFile.write(']}')

    jsonFile.close()

    #####################################################################################

    jsonFile = open("WeeklySchedule.json", "w")

    jsonFile.write('{"Premier": [')
    writeSR(jsonFile, premSchedule)
    jsonFile.write('],')

    jsonFile.write('"Elite": [')
    writeSR(jsonFile, eliteSchedule)
    jsonFile.write('],')

    jsonFile.write('"Major": [')
    writeSR(jsonFile, majorSchedule)
    jsonFile.write('],')

    jsonFile.write('"Minor": [')
    writeSR(jsonFile, minorSchedule)
    jsonFile.write('],')

    jsonFile.write('"Challenger": [')
    writeSR(jsonFile, challSchedule)
    jsonFile.write('],')

    jsonFile.write('"Prospect": [')
    writeSR(jsonFile, prospSchedule)
    jsonFile.write(']}')

    jsonFile.close()

    return

def writeSR(jsonFile, table):

    for i in range(len(table)):
        jsonFile.write("[")
        for j in range(6):
            jsonFile.write('"' + str(table[i][j]) + '"' + ",")

        jsonFile.write("],")

def getSta():

    arrStandings = []

    staSheet = client.open("RSC EU S8 Database").get_worksheet(11)
    staFrSheet = client.open("RSC EU S8 Database").get_worksheet(10)
    staPremier = staSheet.get_values("E3:L12")
    staEliteG = staSheet.get_values("A15:H23")
    staEliteI = staSheet.get_values("A26:H34")
    staMajorG = staSheet.get_values("I15:P23")
    staMajorI = staSheet.get_values("I26:P34")
    staMinorG = staSheet.get_values("A37:H45")
    staMinorI = staSheet.get_values("A48:H56")
    staChallG = staSheet.get_values("I37:P45")
    staChallI = staSheet.get_values("I48:P56")
    staProspG = staSheet.get_values("E59:L67")
    staProspI = staSheet.get_values("E70:L78")
    staFranch = staFrSheet.get_values("E4:L21")

    jsonFile = open("Standings.json", "w")

    jsonFile.write('{"Premier": [')
    writeSta(jsonFile, staPremier)
    jsonFile.write('],')

    jsonFile.write('"EliteG": [')
    writeSta(jsonFile, staEliteG)
    jsonFile.write('],')

    jsonFile.write('"EliteI": [')
    writeSta(jsonFile, staEliteI)
    jsonFile.write('],')

    jsonFile.write('"MajorG": [')
    writeSta(jsonFile, staMajorG)
    jsonFile.write('],')

    jsonFile.write('"MajorI": [')
    writeSta(jsonFile, staMajorI)
    jsonFile.write('],')

    jsonFile.write('"MinorG": [')
    writeSta(jsonFile, staMinorG)
    jsonFile.write('],')

    jsonFile.write('"MinorI": [')
    writeSta(jsonFile, staMinorI)
    jsonFile.write('],')

    jsonFile.write('"ChallG": [')
    writeSta(jsonFile, staChallG)
    jsonFile.write('],')

    jsonFile.write('"ChallI": [')
    writeSta(jsonFile, staChallI)
    jsonFile.write('],')

    jsonFile.write('"ProspG": [')
    writeSta(jsonFile, staProspG)
    jsonFile.write('],')

    jsonFile.write('"ProspI": [')
    writeSta(jsonFile, staProspI)
    jsonFile.write('],')

    jsonFile.write('"Franchise": [')
    writeSta(jsonFile, staFranch)
    jsonFile.write(']}')


    jsonFile.close()

    return arrStandings

def writeSta(jsonFile, table):
    pprint(table)
    for i in range(len(table)):
        jsonFile.write("[")
        for j in range(8):
            jsonFile.write('"' + str(table[i][j]) + '"' + ",")

        jsonFile.write("],")



def getPS(team1, team2, tier):
    conSheet = client.open("RSC EU S8 Contracts").get_worksheet(0)
    dataTN = conSheet.col_values(3)        #Team Names
    dataPN = conSheet.col_values(1)        #Player Names

    dataPS  = tierSheet[tier][0].get_values('A:K')

    bluePlayer = []
    redPlayer = []

    for i in range(len(dataTN)):
        if dataTN[i] == team1:
            bluePlayer.append(dataPN[i])
        if dataTN[i] == team2:
            redPlayer.append(dataPN[i])


    pprint(bluePlayer)
    pprint(redPlayer)

    psArray = []
    bluePlayerNbr = 0
    redPlayerNbr = 28
    bluePlayerHasStats = []
    redPlayerHasStats = []

    if len(bluePlayer) < 4:
        if len(bluePlayer) == 0:
            bluePlayer.append(" ")
        if len(bluePlayer) == 1:
            bluePlayer.append(" ")
        if len(bluePlayer) == 2:
            bluePlayer.append(" ")
        if len(bluePlayer) == 3:
            bluePlayer.append(" ")

    if len(redPlayer) < 4:
        if len(redPlayer) == 0:
            redPlayer.append(" ")
        if len(redPlayer) == 1:
            redPlayer.append(" ")
        if len(redPlayer) == 2:
            redPlayer.append(" ")
        if len(redPlayer) == 3:
            redPlayer.append(" ")

    for i in range(len(dataPS)):
        if i != 0:
            if dataPS[i][1] == bluePlayer[0] or dataPS[i][1] == bluePlayer[1] or dataPS[i][1] == bluePlayer[2] or dataPS[i][1] == bluePlayer[3]:
                print("currently getting Playerstats from: " + dataPS[i][1])
                bluePlayerHasStats.append(dataPS[i][1])
                psArray.insert(0 + bluePlayerNbr, dataPS[i][1]) #Name
                psArray.insert(1 + bluePlayerNbr, dataPS[i][2]) #Games
                psArray.insert(2 + bluePlayerNbr, dataPS[i][7]) #Goals
                psArray.insert(3 + bluePlayerNbr, dataPS[i][8]) #Assists
                psArray.insert(4 + bluePlayerNbr, dataPS[i][9]) #Saves
                psArray.insert(5 + bluePlayerNbr, str(round(int(dataPS[i][7]) / int(dataPS[i][10]) * 100)) + "%") #ShotP
                psArray.insert(6 + bluePlayerNbr, str(round(int(dataPS[i][3]) / int(dataPS[i][2]) * 100)) + "%") #WinP
                bluePlayerNbr += 7
            if dataPS[i][1] == redPlayer[0] or dataPS[i][1] == redPlayer[1] or dataPS[i][1] == redPlayer[2] or dataPS[i][1] == redPlayer[3]:
                print("currently getting Playerstats from: " + dataPS[i][1])
                redPlayerHasStats.append(dataPS[i][1])
                psArray.insert(0 + redPlayerNbr, dataPS[i][1]) #Name
                psArray.insert(1 + redPlayerNbr, dataPS[i][2]) #Games
                psArray.insert(2 + redPlayerNbr, dataPS[i][7]) #Goals
                psArray.insert(3 + redPlayerNbr, dataPS[i][8]) #Assists
                psArray.insert(4 + redPlayerNbr, dataPS[i][9]) #Saves
                psArray.insert(5 + redPlayerNbr, str(round(int(dataPS[i][7]) / int(dataPS[i][10]) * 100)) + "%") #ShotP
                psArray.insert(6 + redPlayerNbr, str(round(int(dataPS[i][3]) / int(dataPS[i][2]) * 100)) + "%") #WinP
                redPlayerNbr += 7

    #Solution for less than 4 players on the roster


    #Solution for Players without stats.
    if len(bluePlayerHasStats) < 5:
        for j in range(len(bluePlayer)):
            blueCount = 0
            if(len(bluePlayerHasStats) == 0):
                print("currently getting Playerstats from: " + bluePlayer[j] + " who has no stats")
                psArray.insert(0 + bluePlayerNbr, bluePlayer[j])  # Name
                psArray.insert(1 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0")  # Games
                psArray.insert(2 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0")  # Goals
                psArray.insert(3 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0")  # Assists
                psArray.insert(4 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0")  # Saves
                psArray.insert(5 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0%")  # ShotP
                psArray.insert(6 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0%")  # WinP
                bluePlayerNbr += 7
            for i in range(len(bluePlayerHasStats)):
                if bluePlayer[j] != bluePlayerHasStats[i]:
                    blueCount += 1
                    if(blueCount == len(bluePlayerHasStats)):
                        print("currently getting Playerstats from: " + bluePlayer[j] + " who has no stats")
                        psArray.insert(0 + bluePlayerNbr, bluePlayer[j])  # Name
                        psArray.insert(1 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0")  # Games
                        psArray.insert(2 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0")  # Goals
                        psArray.insert(3 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0")  # Assists
                        psArray.insert(4 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0")  # Saves
                        psArray.insert(5 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0%")  # ShotP
                        psArray.insert(6 + bluePlayerNbr, " " if bluePlayer[j] == " " else "0%")  # WinP
                        bluePlayerNbr += 7

    if len(redPlayerHasStats) < 5:
        for j in range(len(redPlayer)):
            redCount = 0
            if(len(redPlayerHasStats) == 0):
                print("currently getting Playerstats from: " + redPlayer[j] + " who has no stats")
                psArray.insert(0 + redPlayerNbr, redPlayer[j])  # Name
                psArray.insert(1 + redPlayerNbr, " " if redPlayer[j] == " " else "0")  # Games
                psArray.insert(2 + redPlayerNbr, " " if redPlayer[j] == " " else "0")  # Goals
                psArray.insert(3 + redPlayerNbr, " " if redPlayer[j] == " " else "0")  # Assists
                psArray.insert(4 + redPlayerNbr, " " if redPlayer[j] == " " else "0")  # Saves
                psArray.insert(5 + redPlayerNbr, " " if redPlayer[j] == " " else "0%")  # ShotP
                psArray.insert(6 + redPlayerNbr, " " if redPlayer[j] == " " else "0%")  # WinP
                redPlayerNbr += 7
            for i in range(len(redPlayerHasStats)):
                if redPlayer[j] != redPlayerHasStats[i]:
                     redCount += 1
                     if(redCount == len(redPlayerHasStats)):
                         print("currently getting Playerstats from: " + redPlayer[j] + " who has no stats")
                         psArray.insert(0 + redPlayerNbr, redPlayer[j])  # Name
                         psArray.insert(1 + redPlayerNbr, " " if redPlayer[j] == " " else "0")  # Games
                         psArray.insert(2 + redPlayerNbr, " " if redPlayer[j] == " " else "0")  # Goals
                         psArray.insert(3 + redPlayerNbr, " " if redPlayer[j] == " " else "0")  # Assists
                         psArray.insert(4 + redPlayerNbr, " " if redPlayer[j] == " " else "0")  # Saves
                         psArray.insert(5 + redPlayerNbr, " " if redPlayer[j] == " " else "0%")  # ShotP
                         psArray.insert(6 + redPlayerNbr, " " if redPlayer[j] == " " else "0%")  # WinP
                         redPlayerNbr += 7

    return psArray

def getTS(team1, team2, tier):

    dataStandings = tierSheet[tier][1].get_all_records()
    colStandings = tierSheet[tier][1].col_values(3)
    colTeamStandings = tierSheet[tier][1].col_values(5)

    dataLen = tierSheet[tier][2].get_all_records()
    dataTS  = tierSheet[tier][2].get_values('A:R')

    tsArray = []

    for i in range(len(dataStandings)+1):
        if i != 0:
            if colTeamStandings[i] == team1:
                tsArray.insert(0, colStandings[i]) #Standing

            if colTeamStandings[i] == team2:
                tsArray.insert(9, colStandings[i])

    for i in range(len(dataLen)+1):
        if dataTS[i][1] == team1:
            print("currently getting Stats from team " + dataTS[i][1])
            tsArray.insert(1, dataTS[i][2])
            tsArray.insert(2, dataTS[i][3])
            tsArray.insert(3, dataTS[i][4])
            tsArray.insert(4, dataTS[i][8])
            tsArray.insert(5, dataTS[i][15])
            tsArray.insert(6, dataTS[i][10])
            tsArray.insert(7, dataTS[i][17])
            tsArray.insert(8, dataTS[i][6])

        if dataTS[i][1] == team2:
            print("currently getting Stats from team " + dataTS[i][1])
            tsArray.insert(10, dataTS[i][2])
            tsArray.insert(11, dataTS[i][3])
            tsArray.insert(12, dataTS[i][4])
            tsArray.insert(13, dataTS[i][8])
            tsArray.insert(14, dataTS[i][15])
            tsArray.insert(15, dataTS[i][10])
            tsArray.insert(16, dataTS[i][17])
            tsArray.insert(17, dataTS[i][6])


    return tsArray