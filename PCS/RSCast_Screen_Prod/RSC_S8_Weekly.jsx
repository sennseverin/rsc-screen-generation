#include json2.js
#include TeamInfo.jsx

var WeeklySchedule = loadJson("WeeklySchedule.json")
var WeeklyResult = loadJson("WeeklyResults.json")
var doc = app.activeDocument;

(function main(){
	var weeknbr = 5;

	//doWeekly(tier, weeknbr, doResults, doSchedule)

	doWeeklyPrem(weeknbr, true, true)
	doWeekly("Elite", weeknbr, true, true);
	doWeekly("Major", weeknbr, true, true);
	doWeekly("Minor", weeknbr, true, true);
	doWeekly("Challenger", weeknbr, true, true);
	doWeekly("Prospect", weeknbr, true, true);
	alert("done!");
	
})();

function doWeeklyPrem(week, doResults, doSchedule) {
	var premier = doc.layerSets.getByName("Premier");
	premier.visible = true;
	var confTier = doc.layerSets.getByName("Conference Tiers");
	confTier.visible = false;

	var bg = premier.layerSets.getByName("Background Tier");
	var dataBG = bg.layerSets.getByName("Data Input");

	if(doResults) {
		dataBG.layers[0].textItem.contents = "Premier Week " + week + " Results"
	
		for(var i = 0; i < 5; i++){
			var dataInput = premier.layerSets.getByName("Data Input");
			writeWeeklyResult(dataInput.layers[i], i, WeeklyResult.Premier);
		}
	
		savePNG(doc, "Premier Weekly Results");
	}

	if(doSchedule) {
		dataBG.layers[0].textItem.contents = "Premier Week " + (week + 1) + " Schedule"
	
		for(var i = 0; i < 5; i++){
			var dataInput = premier.layerSets.getByName("Data Input");
			writeWeeklySchedule(dataInput.layers[i], i, WeeklySchedule.Premier);
		}
	
		savePNG(doc, "Premier Weekly Schedule");
	}
}

function doWeekly(tier, week, doResults, doSchedule) {

	var premier = doc.layerSets.getByName("Premier");
	premier.visible = false;
	var confTier = doc.layerSets.getByName("Conference Tiers");
	confTier.visible = true;


	var bg = confTier.layerSets.getByName("Background Tier");
	var dataBG = bg.layerSets.getByName("Data Input");
	var tiles = dataBG.layerSets.getByName("Tiles");
	var glacies = confTier.layerSets.getByName("Glacies");
	var ignis = confTier.layerSets.getByName("Ignis");
	var crossConf = confTier.layerSets.getByName("CrossConference");

	for(var i = 0; i < 5; i++) {
		tiles.layers[i].visible = false;
	}
	switch(tier) {
		case "Elite": 
			dataR = WeeklyResult.Elite; 
			dataS = WeeklySchedule.Elite;
			break;
		case "Major": 
			dataR = WeeklyResult.Major; 
			dataS = WeeklySchedule.Major;
			break;
		case "Minor": 
			dataR = WeeklyResult.Minor; 
			dataS = WeeklySchedule.Minor;
			break;
		case "Challenger": 
			dataR = WeeklyResult.Challenger; 
			dataS = WeeklySchedule.Challenger;
			break;
		case "Prospect": 
			dataR = WeeklyResult.Prospect; 
			dataS = WeeklySchedule.Prospect;
			break;
	}

	if(doResults) {

		tiles.layerSets.getByName(tier).visible = true;
		dataBG.layers[0].textItem.contents = tier + " Week " + week + " Results"
	
		var ignisNbr = 0;
		var glaciesNbr = 0;
		var ccNbr = 0;
	
		for(var i = 0; i < 18; i++){
	
			if(teamInfo[dataR[i][2]][1] == teamInfo[dataR[i][5]][1]){
	
				if(teamInfo[dataR[i][2]][1] == "Ignis") {
					var dataInput = ignis.layerSets.getByName("Data Input");
					writeWeeklyResult(dataInput.layers[ignisNbr], i, dataR);
					ignisNbr++;
				}
		
				if(teamInfo[dataR[i][2]][1] == "Glacies") {
					var dataInput = glacies.layerSets.getByName("Data Input");
					writeWeeklyResult(dataInput.layers[glaciesNbr], i, dataR);
					glaciesNbr++;
				}
			}
			else {
				writeWeeklyResult(crossConf.layers[ccNbr], i, dataR);
				ccNbr++;
			}	
		}

		savePNG(doc, tier + " Weekly Results");

	}

	if(doSchedule) {

		tiles.layerSets.getByName(tier).visible = true;
		dataBG.layers[0].textItem.contents = tier + " Week " + (week + 1) + " Schedule"
	
		ignisNbr = 0;
		glaciesNbr = 0;
		ccNbr = 0;
		
	
		for(var i = 0; i < 18; i++){
			if(teamInfo[dataS[i][2]][1] == teamInfo[dataS[i][5]][1]){
	
				if(teamInfo[dataS[i][2]][1] == "Ignis") {
					var dataInput = ignis.layerSets.getByName("Data Input");
					writeWeeklySchedule(dataInput.layers[ignisNbr], i, dataS);
					ignisNbr++;
				}
		
				if(teamInfo[dataS[i][2]][1] == "Glacies") {
					var dataInput = glacies.layerSets.getByName("Data Input");
					writeWeeklySchedule(dataInput.layers[glaciesNbr], i, dataS);
					glaciesNbr++;
				}
			}
			else {
				writeWeeklySchedule(crossConf.layers[ccNbr], i, dataS);
				ccNbr++;
			}	
		}
	
		savePNG(doc, tier + " Weekly Schedule");
	}
}

function writeWeeklyResult(group, number, jsonInfo) {
	group.layers[0].textItem.contents = jsonInfo[number][3] + ":" + jsonInfo[number][4];
	group.layers[3].textItem.contents = jsonInfo[number][2];
	group.layers[6].textItem.contents = jsonInfo[number][5];
	setTeamLogo(group.layers[2], teamInfo[jsonInfo[number][2]][0]);
	setTeamLogo(group.layers[5], teamInfo[jsonInfo[number][5]][0]);
	
}

function writeWeeklySchedule(group, number, jsonInfo) {
	group.layers[0].textItem.contents = jsonInfo[number][0];
	group.layers[3].textItem.contents = jsonInfo[number][2];
	group.layers[6].textItem.contents = jsonInfo[number][5];
	setTeamLogo(group.layers[2], teamInfo[jsonInfo[number][2]][0]);
	setTeamLogo(group.layers[5], teamInfo[jsonInfo[number][5]][0]);
	
}

function setTeamLogo(logoGroup, teamLogo){
	for (var i = 0; i < logoGroup.layers.length; i++) { 	
		logoGroup.layers[i].visible = false;
	}
	logoGroup.layers[teamLogo].visible = true; 
}

function loadJson(relPath) {
	var script = new File($.fileName);
	var jsonFile = new File(script.path + '/' + relPath);

	jsonFile.open('r');
	var str = jsonFile.read();
	jsonFile.close();

	return JSON.parse(str)
}

function savePNG(doc, name) {
	var file = new File(doc.path + '/Output/' + name + '.PNG');

	var opts = new PNGSaveOptions();
	opts.quality = 10;

	doc.saveAs(file, opts, true);
}